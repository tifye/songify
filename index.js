const express = require('express');
const expressHandlebars = require('express-handlebars');

const app = express();

const handlebars = expressHandlebars.create({
  extname: '.hbs'
});

app.engine('.hbs', handlebars.engine);
app.set('view engine', '.hbs');

app.get('/', (req, res) => {
  res.render('home');
});

app.listen(8080, () => {
  console.log('meep');
});